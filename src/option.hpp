#ifndef MENUPP_OPTION_HPP_
#define MENUPP_OPTION_HPP_

#include "node.hpp"

#include <ostream>
#include <iterator>

namespace Menu
{

    template<typename F>
    class Option: public INode
    {
        NodeId _id;
        const char* _label;
        F _f;
    
    public:
        explicit Option(const char* label, F f)
        : _label(label), _f(f) {}
    
        NodeId id() const override { return _id; }
    
        void set_id(NodeId id) override { _id = id; }
    
        void dump(std::ostream& os, size_t indent, size_t total_indent) const override
        {
            (void) indent;
            std::fill_n(std::ostream_iterator<char>(os), total_indent, ' ');
            os << "Option" << _id << ": \"" << label() << "\"\n";
        }
    
        NodeId move(Move m) override
        {
            switch (m) {
                case Move::Into:
                    _f();
                    return _id.parent();
                case Move::Up:
                case Move::Down:
                case Move::Left:
                case Move::Right:
                    return NodeId();
            }
            return NodeId();
        }
    
        INode* get(size_t) override { return nullptr; }
    
        const char* label() const override { return _label; }
    };
    
}

#endif // MENUPP_OPTION_HPP_
