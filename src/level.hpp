#ifndef MENUPP_LEVEL_HPP_
#define MENUPP_LEVEL_HPP_

#include "node.hpp"

namespace Menu
{

    template<
        typename Conf = LevelConf<>,
        typename FRead = void(*)(typename Conf::Type),
        typename FCallback = void(*)(typename Conf::Type)
    >
    class Level : public INode
    {
        NodeId _id;
        const char* _label;
        typename Conf::Type _value = Conf::dflt;
        typename Conf::Type _value_save = Conf::dflt;
        FCallback _fc;
        FRead _fr;
    
    public:
        explicit Level(const char* label, FCallback fc, FRead fr)
        : _label(label), _fc(fc), _fr(fr) {}
    
        void set_id(NodeId id) override { _id = id; }
    
        NodeId id() const override { return _id; }
    
        void dump(std::ostream& os, size_t indent, size_t total_indent) const override
        {
            std::fill_n(std::ostream_iterator<char>(os), total_indent, ' ');
            os << "Level" << _id << ": \"" << label() << "\"\n";
            std::fill_n(std::ostream_iterator<char>(os), total_indent + indent, ' ');
            os << _value << '\n';
        }
    
        NodeId move(Move m) override
        {
            NodeId ret = _id;
            switch (m) {
                case Move::Into:
                    _value_save = _value;
                    _fr(_value);
                    break;
                case Move::Up:
                    _value = inc<Conf>(_value);
                    _fr(_value);
                    break;
                case Move::Down:
                    _value = dec<Conf>(_value);
                    _fr(_value);
                    break;
                case Move::Left:
                    _value = _value_save;
                    ret = _id.parent();
                    break;
                case Move::Right:
                    _fc(_value);
                    ret = _id.parent();
                    break;
            }
            return ret;
        }
    
        INode* get(size_t) override { return nullptr; }
    
        const char* label() const override { return _label; }
    };

}

#endif // MENUPP_LEVEL_HPP_
