#ifndef MENUPP_LIST_HPP_
#define MENUPP_LIST_HPP_

#include "node.hpp"

#include <tuple>

namespace Menu
{

    template<size_t N, typename List, typename Tuple>
    constexpr void configure_node(List* sn, Tuple& t)
    {
        if constexpr (N > 0) {
            configure_node<N - 1>(sn, t);
        }
        std::get<N>(t).set_id({ sn, N });
    }

    template<typename List, typename Tuple>
    constexpr void configure_nodes(List* sn, Tuple& t)
    { configure_node<std::tuple_size<Tuple>::value - 1>(sn, t); }

    template<size_t N, typename NodeTuple>
    constexpr void dump_nodes(std::ostream& os, NodeTuple& t, size_t indent, size_t total_indent)
    {
        if constexpr (N > 0) {
            dump_nodes<N - 1>(os, t, indent, total_indent);
        }
        std::get<N>(t).dump(os, indent, total_indent);
    }

    template<typename NodeTuple>
    constexpr void dump_nodes(std::ostream& os, NodeTuple& t, size_t indent, size_t total_indent)
    { dump_nodes<std::tuple_size<NodeTuple>::value - 1>(os, t, indent, total_indent); }

    template<typename Tuple, typename Node, size_t... Is>
    constexpr auto make_node_getter(std::index_sequence<Is...>)
    {
        return [](Tuple& t, size_t i) -> Node* {
            if (i < sizeof...(Is)) {
                return std::array<Node*, sizeof...(Is)>{{ (&(std::get<Is>(t)))... }}[i];
            } else {
                return nullptr;
            }
        };
    }

    template<typename F, typename... Nodes>
    class List : public INode
    {
        NodeId _id;
        const char* _label;
        std::tuple<Nodes...> _childs;
        F _f;
        size_t _i = 0;
    
        using Class = List<Nodes...>;
        using Childs = decltype(_childs);
        static constexpr size_t size = std::tuple_size<Childs>::value;
    
        static constexpr auto get_node =
            make_node_getter<Childs, INode>(std::index_sequence_for<Nodes...>{});
        static constexpr auto get_const_node =
            make_node_getter<const Childs, const INode>(std::index_sequence_for<Nodes...>{});
    
    public:
        List(List&& sn) = default;
        List(const List&) = default;
    
        template<
            typename = std::enable_if_t<(sizeof...(Nodes) > 0)>,
            typename = Extends<INode, Nodes...>
        >
        explicit List(const char* label, F f, Nodes&&... ns)
        : _label(label), _childs(std::move(ns)...), _f(f) {}
    
        ~List() = default;
    
        void set_id(NodeId id) override
        {
            _id = id;
            configure_nodes(this, _childs);
        }
    
        NodeId id() const override { return _id; }
    
        void dump(std::ostream& os, size_t indent, size_t total_indent) const override
        {
            std::fill_n(std::ostream_iterator<char>(os), total_indent, ' ');
            os << "List" << _id << ": \"" << label() << "\"\n";
            dump_nodes(os, _childs, indent, total_indent + indent);
        }
    
        NodeId move(Move m) override
        {
            NodeId ret = _id;
            switch (m) {
                case Move::Into:
                    _f(get_const_node(_childs, _i)->label());
                    break;
                case Move::Up:
                    _i = dec<LevelConf<Range<size_t, 0, size - 1>, 0, 1, true>>(_i);
                    _f(get_const_node(_childs, _i)->label());
                    break;
                case Move::Down:
                    _i = inc<LevelConf<Range<size_t, 0, size - 1>, 0, 1, true>>(_i);
                    _f(get_const_node(_childs, _i)->label());
                    break;
                case Move::Left:
                    ret = _id.parent();
                    break;
                case Move::Right:
                    ret = { this, _i };
                    break;
            }
            return ret;
        }
    
        INode* get(size_t n) override { return get_node(_childs, n); }
    
        const char* label() const override { return _label; }
    };

}

#endif // MENUPP_LIST_HPP_
