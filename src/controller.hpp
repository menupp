#ifndef MENUPP_CONTROLLER_HPP_
#define MENUPP_CONTROLLER_HPP_

#include "node.hpp"

namespace Menu
{

    template<typename Node>
    class Controller
    {
        Node _root;
        NodeId _position = NodeId::ROOT;
    
        friend std::ostream& operator<<(std::ostream& os, const Controller<Node>& menu)
        { return os << menu._root; }
    
    public:
        Controller(Node&& n)
        : _root(std::move(n))
        {
            _root.set_id(_position);
            move(Move::Into);
        }
    
        bool move(Move m)
        {
            INode* cur = _position.get(_root);
            if (cur) {
                NodeId id = cur->move(m);
                if (id.is_valid() && _position != id) {
                    _position = id;
                    return move(Move::Into);
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }
        
    };

}

#endif // MENUPP_CONTROLLER_HPP_
