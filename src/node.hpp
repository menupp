#ifndef MENUPP_NODE_HPP_
#define MENUPP_NODE_HPP_

#include "common.hpp"

namespace Menu
{

    struct INode;

    class NodeId
    {
        INode* _ptr = nullptr;
        int _n_child = -1;

    public:
        NodeId() {}
        NodeId(INode* ptr, size_t n_child)
        : _ptr(ptr), _n_child(static_cast<int>(n_child))
        {}

        static const NodeId ROOT;

        bool is_valid() const { return _n_child >= 0; }

        bool is_root() const { return is_valid() && !_ptr; }

        INode* addr() const { return _ptr; }

        int n() const { return _n_child; }

        NodeId parent() const;

        INode* get(INode& root);

        friend std::ostream& operator<<(std::ostream& os, const NodeId& nid)
        { return os << '(' << nid._ptr << ", " << nid._n_child << ')'; }

        friend bool operator==(const NodeId& x, const NodeId& y)
        { return x._ptr == y._ptr && x._n_child == y._n_child; }

        friend bool operator!=(const NodeId& x, const NodeId& y)
        { return !(x == y); }
    };

    const NodeId NodeId::ROOT { nullptr, 0 };

    struct INode
    {
        virtual ~INode() = default;
        virtual void set_id(NodeId id) = 0;
        virtual NodeId id() const = 0;
        virtual void dump(std::ostream& os, size_t indent = 4, size_t total_indent = 0) const = 0;
        virtual NodeId move(Move m) = 0;
        virtual INode* get(size_t n) = 0;
        virtual const char* label() const = 0;

        friend std::ostream& operator<<(std::ostream& os, const INode& node);
    };

    NodeId NodeId::parent() const
    {
        return (_ptr
            ? _ptr->id()
            : (is_valid()
                ? (is_root() ? NodeId() : ROOT)
                : NodeId()));
    }

    INode* NodeId::get(INode& root)
    {
        return is_valid()
            ? (is_root()
                ? &root
                : _ptr->get(static_cast<size_t>(_n_child)))
            : nullptr;
    }

    std::ostream& operator<<(std::ostream& os, const INode& node)
    { node.dump(os, 4); return os; }

}

#endif // MENUPP_NODE_HPP_
