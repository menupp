#ifndef MENUPP_COMMON_HPP_
#define MENUPP_COMMON_HPP_

namespace Menu
{

    template<typename B, typename... Ts>
    using Extends = typename std::enable_if<(... && std::is_base_of<B, Ts>::value)>::type;

    enum struct Move
    {
        Into, Up, Down, Left, Right
    };

    template<typename T = size_t, T Min = 0, T Max = 100>
    struct Range
    {
        static_assert(Min <= Max);
    
        using Type = T;
        static constexpr Type min = Min;
        static constexpr Type max = Max;
    };

    template<
        typename R = Range<>,
        typename R::Type Default = 0,
        typename R::Type Inc = 1,
        bool Loop = false
    >
    struct LevelConf
    {
        static_assert(Inc <= (R::max - R::min));
        static_assert(Default >= R::min);
        static_assert(Default <= R::max);
    
        using Type = typename R::Type;
        using Range = R;
        static constexpr bool loop = Loop;
        static constexpr Type inc = Inc;
        static constexpr Type dflt = Default;
    };

    template<typename LevelConf>
    typename LevelConf::Type inc(typename LevelConf::Type x)
    {
        typename LevelConf::Type ret = x + LevelConf::inc;
        if constexpr(LevelConf::loop) {
            if (ret < x)
                ret = LevelConf::Range::min;
            else if (ret > LevelConf::Range::max)
                ret = LevelConf::Range::min;
            else if (ret < LevelConf::Range::min)
                ret = LevelConf::Range::max;
        } else {
            if (ret < x)
                ret = LevelConf::Range::max;
            else if (ret > LevelConf::Range::max)
                ret = LevelConf::Range::max;
            else if (ret < LevelConf::Range::min)
                ret = LevelConf::Range::min;
        }
        return ret;
    }

    template<typename LevelConf>
    typename LevelConf::Type dec(typename LevelConf::Type x)
    {
        typename LevelConf::Type ret = x - LevelConf::inc;
        if constexpr(LevelConf::loop) {
            if (ret > x)
                ret = LevelConf::Range::max;
            else if (ret > LevelConf::Range::max)
                ret = LevelConf::Range::min;
            else if (ret < LevelConf::Range::min)
                ret = LevelConf::Range::max;
        } else {
            if (ret > x)
                ret = LevelConf::Range::min;
            else if (ret > LevelConf::Range::max)
                ret = LevelConf::Range::max;
            else if (ret < LevelConf::Range::min)
                ret = LevelConf::Range::min;
        }
        return ret;
    }

}

#endif // MENUPP_COMMON_HPP_
